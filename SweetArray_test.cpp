/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/
#include <iostream>
#include "SweetArray.h"
#include <cstdlib>		//random numbers
#include <ctime>		//time function
using namespace std;

int main()
{
	int size = 5;
	
	srand(time(NULL));

	SweetArray A(size), B(size), C(size), D(size), Intersection, Union;

	for(int c = 0; c < size; c++)
	{
		A[c] = rand() % 10;
		B[c] = rand() % 10;
		D[c] = 1;
	}

    C = A + B;
	

	cout << "Set A:\t" << A << endl 
		 << "Set B:\t" << B << endl
		 << "Set C:\t" << C << endl;
	
	Intersection = (A & B);
	
	cout << "The Intersection of A and B:\t" << Intersection << endl;
	
	Intersection = (A & C);
	
	cout << "The Intersection of A and C:\t" << Intersection << endl;
	
	Intersection = (B & C);
		 
	cout << "The Intersection of B and C:\t" << Intersection << endl;
	
	Union = (A | B);
	
	cout << "The Union of A and B:\t" << Union << endl;
	
	Union = (A | C);
	
	cout << "The Union of A and C:\t" << Union << endl;
	
	Union = (B | C);
	
	cout << "The Union of B and C:\t" << Union << endl;

	Intersection = (D & D);
	
	cout << Intersection << endl;
	
	return 0;

}
